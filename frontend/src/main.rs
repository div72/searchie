#[macro_use] extern crate rocket;

use std::time::Instant;

use clap::Parser;
use rocket::fs::FileServer;
use rocket_dyn_templates::{Template, context};
use rocket_db_pools::{sqlx, sqlx::Row, Connection, Database};

#[derive(Database)]
#[database("index")]
struct Index(sqlx::SqlitePool);

#[get("/")]
fn index() -> Template {
    Template::render("index", context! [])
}

// FIXME: | safe in creates HTML injection probably.
// TODO: ret value should be Option -> Result
#[get("/search?<q>&<tags>")]
async fn search(q: &str, tags: Option<&str>, mut db: Connection<Index>) -> Option<Template> {
    let start = Instant::now();
    let res = sqlx::query("SELECT url, title, snippet(page_fts5, 1, '<b>', '</b>', '...', 32) FROM page_fts5 WHERE page_fts5 MATCH ? ORDER BY rank LIMIT 10;")
        .bind(q)
        .fetch_all(&mut **db)
        .await
        .unwrap();

    let mut results: Vec<(String, String, String)> = Vec::new();

    for row in res {
        results.push((row.try_get::<String, usize>(0).ok()?, row.try_get::<String, usize>(1).ok()?, row.try_get::<String, usize>(2).ok()?));
    }
    Some(Template::render("search", context! { results: results, elapsed: start.elapsed().as_millis() }))
}

#[derive(Debug, clap::Parser)]
#[command(version)]
struct Options {
    /// Path for the sqlite database.
    #[arg(long, default_value = "/var/lib/searchie/index.sqlite")]
    database_path: String,
}

#[launch]
fn rocket() -> _ {
    let args = Options::parse();

    let config = rocket::Config::figment()
        .merge(("databases.index", rocket_db_pools::Config {
            url: args.database_path.into(),
            connect_timeout: 3,
            min_connections: None,
            max_connections: 1024,
            idle_timeout: None,
        }));
    rocket::custom(config)
        .attach(Index::init())
        .attach(Template::fairing())
        .mount("/static", FileServer::from("./static"))
        .mount("/", routes![index, search])
}
