#![feature(async_closure)]

use log::info;

use std::collections::HashSet;
use std::fmt::Write;
use std::time::Instant;

use clap::Parser;

use env_logger::Env;

use futures::stream::{FuturesUnordered, StreamExt};

use spider::compact_str::ToCompactString;
use spider::packages::scraper::{Html, Selector, Node, Node::Text};
use spider::tokio;
use spider::website::Website;

use sqlx::Row;
use sqlx::SqlitePool;

const DEFAULT_SCRAPE_DELAY_MS: u64 = 250;
const USER_AGENT: &str = concat!("searchie/", env!("CARGO_PKG_VERSION"), " (https://codeberg.org/div72/searchie)");
const INDEX_BACKOFF: i64 = 7 * 24 * 60 * 60;

const fn default_scrape_delay_ms() -> u64 {
    DEFAULT_SCRAPE_DELAY_MS
}

#[derive(Debug, serde::Deserialize)]
struct Site {
    url: String,
    #[serde(default = "default_scrape_delay_ms")]
    scrape_delay_ms: u64,
    tags: Vec<String>,
    blacklisted_paths: Option<Vec<String>>,
}

#[derive(Debug, serde::Deserialize)]
struct Sites {
    sites: Vec<Site>
}

#[derive(Debug, Parser)]
#[command(version)]
struct Options {
    /// Path for the sqlite database.
    #[arg(long, default_value = "/var/lib/searchie/index.sqlite")]
    database_path: String,

    /// Path for the file containing websites to index.
    #[arg(long, default_value = "/etc/searchie/sites.txt")]
    sites_path: String,
}

struct HTMLExtractor {
    title: Option<String>,
    body: String
}

impl HTMLExtractor {
    fn new() -> HTMLExtractor {
        HTMLExtractor {
            title: None,
            body: String::with_capacity(16384)
        }
    }

    fn process_recursive(&mut self, node: ego_tree::NodeRef<Node>) {
        let value = node.value();

        let mut end_str = "";
        match value {
            Node::Document | Node::Doctype(_) => {}
            Node::Element(element) => {
                match element.name() {
                    "button" | "footer" | "form" | "header" | "img" | "input" | "meta" | "nav" | "style" | "svg" | "textarea" | "video" => {
                        return;
                    }
                    "a" | "b" | "i" | "span" => {
                        self.body.push_str(" ");
                        end_str = " ";
                    }
                    "p" => {
                        self.body.push_str("\n");
                        end_str = "\n";
                    }
                    "br" | "div" => {
                        self.body.push_str("\n");
                    }
                    "title" => {
                        if let Some(child) = node.children().next() {
                            if let Node::Text(text) = child.value() {
                                self.title = Some(text.to_string());
                            }
                        }
                        return;
                    }
                    _ => {}
                }
            }
            Node::Text(text) => {
                self.body.push_str(&text.trim());
            }
            _ => {
                println!("{:#?}", value)
            }
        }

        for child in node.children() {
            self.process_recursive(child);
        }

        self.body.push_str(end_str);
    }

    fn process(&mut self, html: &str) {
        let document = Html::parse_document(html);

        self.process_recursive(document.tree.root());
    }
}

#[tokio::main]
async fn main() {
    let start = Instant::now();
    let options = Options::parse();

    env_logger::init_from_env(
        Env::default()
            .filter_or("RUST_LOG", "info")
            .write_style_or("RUST_LOG_STYLE", "always")
    );

    println!("{:#?}", options);
    let sites: Sites = serde_yaml::from_str(&std::fs::read_to_string(&options.sites_path).unwrap()).unwrap();
    println!("{:#?}", sites);

    if !std::path::Path::new(&options.database_path).is_file() {
        std::fs::File::create(&options.database_path).unwrap();
    }

    let database_uri = "sqlite:".to_owned() + &options.database_path;
    let pool = SqlitePool::connect(&database_uri).await.unwrap();

    sqlx::query(
        "
CREATE TABLE IF NOT EXISTS page (
    url TEXT UNIQUE NOT NULL,
    last_indexed INTEGER NOT NULL,

    title TEXT NOT NULL,
    body TEXT NOT NULL
) STRICT;

CREATE TABLE IF NOT EXISTS tag (
    tag TEXT NOT NULL,

    page_url TEXT NOT NULL REFERENCES page(url)
) STRICT;

CREATE VIRTUAL TABLE IF NOT EXISTS page_fts5 USING FTS5(
    title,
    body,
    url UNINDEXED,
    content='page'
);

CREATE TRIGGER IF NOT EXISTS user_insert AFTER INSERT ON page BEGIN
    INSERT INTO page_fts5(rowid, title, body, url) VALUES(new.rowid, new.title, new.body, new.url);
END;

CREATE TRIGGER IF NOT EXISTS user_delete AFTER DELETE ON page BEGIN
    INSERT INTO page_fts5(rowid, page_fts5) VALUES(old.rowid, 'delete');
END;

CREATE TRIGGER IF NOT EXISTS user_update AFTER UPDATE ON page BEGIN
    INSERT INTO page_fts5(rowid, page_fts5) VALUES(old.rowid, 'delete');
    INSERT INTO page_fts5(rowid, title, body, url) VALUES(new.rowid, new.title, new.body, new.url);
END;
    ",
    )
    .execute(&pool)
    .await
    .unwrap();

    let mut websites_to_be_scraped = FuturesUnordered::new();

    info!("Preparing sites...");
    for site in &sites.sites {
        let mut website = Box::new(Website::new(&site.url));

        website.configuration.respect_robots_txt = true;
        website.configuration.subdomains = false;
        website.configuration.delay = site.scrape_delay_ms;
        website.configuration.user_agent = Some(Box::new(USER_AGENT.to_compact_string()));
        website.with_blacklist_url(
            Some(
                site.blacklisted_paths
                .clone()
                .unwrap_or(vec![])
                .iter()
                .map(|s| s.to_compact_string())
                .collect::<Vec<_>>()
            )
        );

        // FIXME: Is this legal?
        let links_visited: *mut spider::hashbrown::HashSet<spider::CaseInsensitiveString> = website.get_links() as *const spider::hashbrown::HashSet<spider::CaseInsensitiveString> as *mut spider::hashbrown::HashSet<spider::CaseInsensitiveString>;

        for link in sqlx::query("SELECT url FROM page WHERE last_indexed + ? > unixepoch() AND url LIKE ? || '%';")
            .bind(INDEX_BACKOFF)
            .bind(website.get_absolute_path(None).unwrap().to_string())
            .fetch_all(&pool)
            .await
            .unwrap()
            .into_iter()
            .map(|row| row.try_get::<&str, &str>("url").unwrap().to_compact_string()) {
            unsafe { links_visited.as_mut() }.unwrap().insert(link.into());
        }

        websites_to_be_scraped.push((async move || ((*website).scrape().await, website))());
    }

    let title_selector = Selector::parse("title").unwrap();

    info!("Waiting for sites to be scraped...");
    loop {
        match websites_to_be_scraped.next().await {
            Some((_, website)) => {
                let pages = website.get_pages();

                if pages.is_none() {
                    println!("[WARN] Site returned no pages: {:?}", website.get_url());
                    continue;
                }

                for page in pages.unwrap().iter() {
                    println!("Processing {}", page.get_url());

                    let html = page.get_html();
                    let mut extractor = HTMLExtractor::new();
                    extractor.process(&html);

                    // INSERT OR REPLACE breakes the triggers, must use ON CONFLICT here.
                    sqlx::query(
                        r#"
INSERT INTO page(url, last_indexed, title, body) VALUES(?, unixepoch(), ?, ?)
    ON CONFLICT(url) DO UPDATE SET last_indexed=unixepoch(),title=excluded.title,body=excluded.body;
                "#,
                    )
                    .bind(page.get_url())
                    .bind(extractor.title.unwrap_or_else(|| "No title".to_owned()))
                    .bind(&extractor.body)
                    .execute(&pool)
                    .await
                    .unwrap();
                }
            }
            None => {
                break;
            }
        }
    }

    println!("Finished crawl in {:?}", start.elapsed());
    println!("Applying tags.");

    sqlx::query(
        "DELETE FROM tag;"
    )
    .execute(&pool)
    .await
    .unwrap();

    for site in &sites.sites {
        let mut query_builder: sqlx::QueryBuilder<sqlx::Sqlite> = sqlx::QueryBuilder::new(
            "INSERT INTO tag(tag, page_url) SELECT ?, url FROM page WHERE url LIKE '%' || ? || '%';"
        );
        query_builder.push_values(&site.tags, |mut builder, tag| {
            builder.push_bind(tag)
                .push_bind(&site.url);
        });
        query_builder.build()
            .execute(&pool)
            .await
            .unwrap();
    }
}
